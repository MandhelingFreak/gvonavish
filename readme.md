﻿# GVONavish

『大航海時代 Online』に関わる著作権、その他一切の知的財産権は、株式会社コーエーテクモゲームスに帰属します。

---

GVONaviが極北でちゃんと動かなくなったので、GVONaviっぽく動くアプリを模倣して作ってみました。

オープンソースなので今後のアップデートで放置されても有志が変更できます。


## 開発環境

* C/C++
* Visual Studio 2013 Express Update 1
* Windows 7

VS2013以降があればそのままビルドできるはずです。

別途ランタイムのインストールが必要な外部ライブラリは使用していません。

## デバッグの仕方

最初に地図画像ファイルを選択してください。
添付地図はassetsフォルダに入っています。
終了すると実行ファイルと同じ場所に設定ファイルが作成されます。
Debug/Releaseで同じ設定を使いたい場合はコピーしてください。

## パッケージング

archiveの下にあるGVONavishフォルダを圧縮して配布するとよろしいかと。

## ライセンス

本ソースコードはGPLv2でライセンスされています。

本ソースコードを元に派生したソフトウェアはGPLV2が適用されます。

添付されている地図はDOL統括wikiで公開されているリソースを利用しているものなのでGPLから除外となります。

## 謝辞

* 測量座標解析は交易マップC#のソースを参考にしました。
* 世界地図は統括wikiの最新版を使用しました。